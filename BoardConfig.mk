#
# Copyright (C) 2016 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include device/cyanogen/msm8916-common/BoardConfigCommon.mk

include device/vestel/v3/board/*.mk

TARGET_BOARD_INFO_FILE := device/vestel/v3/board-info.txt

# inherit from the proprietary version
-include vendor/vestel/v3/BoardConfigVendor.mk
TARGET_PREBUILT_KERNEL := device/vestel/v3/kernel.gz
BOARD_KERNEL_BASE := 0x80000000
BOARD_KERNEL_PAGESIZE := 2048
BOARD_KERNEL_CMDLINE := androidboot.hardware=qcom msm_rtb.filter=0x237 ehci-hcd.park=3 androidboot.bootdevice=7824900.sdhci lpm_levels.sleep_disabled=1 earlyprintk
BLOCK_BASED_OTA := false
TARGET_DEVICE := v3
USE_CCACHE=1
CCACHE_DIR=/home/kutlay1653/Desktop/cache

