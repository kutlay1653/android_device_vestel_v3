LOCAL_PATH := $(call my-dir)

#----------------------------------------------------------------------
# extra images
#----------------------------------------------------------------------
include build/core/generate_extra_images.mk
ALL_PREBUILT += $(INSTALLED_KERNEL_TARGET)

ifneq ($(TARGET_PREBUILT_KERNEL),)
$(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr:
	mkdir -p $@
endif
